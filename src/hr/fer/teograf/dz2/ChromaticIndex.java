package hr.fer.teograf.dz2;

import java.io.File;
import java.io.IOException;
import java.util.*;

public class ChromaticIndex {

    public static final int NOT_COLORED = 0;

    public static void main(String[] args) {

        if (args.length != 1) {
            System.err.println("Need filename for argument! Aborting.");
            System.exit(-1);
        }

        String filename = args[0];

        Graph g = new Graph();

        // edge weight is used to store the coloring information
        // initially, none of the edges are colored
        int weight = NOT_COLORED;

        try (Scanner scanner = new Scanner(new File(filename))) {

            int vertexCount = scanner.nextInt();

            if (vertexCount < 2) {
                System.out.print("0");
                return;
            } else if (vertexCount == 2) {
                System.out.print("1");
                return;
            }

            Vertex first;
            Vertex second;
            Edge edge;

            for (int i = 0; i < vertexCount; ++i) {

                for (int j = 0; j < vertexCount; ++j) {

                    int neighbours = scanner.nextInt();

                    if (j < i) {
                        continue;
                    }

                    if (neighbours == 1) {

                        first = new Vertex("v" + Integer.toString(i));
                        second = new Vertex("v" + Integer.toString(j));

                        // prevent duplicate instances of the same vertex
                        if (g.getVertices().contains(first)) {
                            first = g.getVertex(first);
                        }

                        if (g.getVertices().contains(second)) {
                            second = g.getVertex(second);
                        }

                        edge = new Edge(weight, first, second);

                        if (g.getEdges().contains(edge)) {
                            edge = g.getEdge(edge);
                        }

                        g.addEdge(edge);
                    }
                }
            }

            // add isolated vertices if they exist - only matters for graphs that are not connected
            for (int i = 0; i < vertexCount - g.getVertices().size(); i++) {
                g.addVertex(new Vertex("_v" + i));
            }

            List<Edge> edges = new ArrayList<>(g.getEdges());

            int maxDegree = g.getMaxDegree();
            if (colorEdge(edges, 0, maxDegree)) {
                System.out.print(Integer.toString(maxDegree));
            } else {
                for (Edge e : edges) {
                    e.setWeight(NOT_COLORED);
                }
                colorEdge(edges, 0, maxDegree + 1);
                System.out.print(Integer.toString(maxDegree + 1));
            }

            // print the colored edges
//            for (Edge e : g.getEdges()) {
//                System.out.println(e.toString());
//            }

        } catch (IOException e) {
            e.printStackTrace();
        }

    }

    /**
     * returns true if the graph can be colored with maxColors colors
     */
    private static boolean colorEdge(List<Edge> edges, int index, int maxColors) {

        Edge currentEdge = edges.get(index);

        List<Integer> availableColors = getAvailableColors(currentEdge, maxColors);

        // we ran out of colors
        if (availableColors.isEmpty()) {
            return false;
        } else if (index == edges.size() - 1) {
            currentEdge.setWeight(availableColors.get(0));
            return true;
        }

        for (Integer availableColor : availableColors) {
            // remove and add back after weight changes because it impacts hashCode()
            currentEdge.setWeight(availableColor);

            if (index < edges.size() - 1) {
                if (colorEdge(edges, index + 1, maxColors)) {
                    // found a solution, climb up the recursion tree
                    return true;
                }
            }
        }

        // no solution was found in this subtree of the recursion
        currentEdge.setWeight(NOT_COLORED);
        return false;
    }

    private static List<Integer> getAvailableColors(Edge edge, int maxColors) {
        Set<Edge> neighbourEdges = new HashSet<>(edge.getFirst().getEdges().size() + edge.getSecond().getEdges()
                .size());
        neighbourEdges.addAll(edge.getFirst().getEdges());
        neighbourEdges.addAll(edge.getSecond().getEdges());
        neighbourEdges.remove(edge);

        List<Integer> availableColors = new ArrayList<>(maxColors);
        for (int i = 1; i <= maxColors; i++) {
            availableColors.add(i);
        }

        for (Edge neighbourEdge : neighbourEdges) {
            availableColors.remove(new Integer(neighbourEdge.getWeight()));
        }

        return availableColors;
    }

}
